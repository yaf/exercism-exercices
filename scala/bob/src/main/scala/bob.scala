class Bob {
  def hey(sentence: String): String =
    if (sentence == sentence.toUpperCase && sentence != sentence.toLowerCase) "Woah, chill out!"
    else if (sentence.endsWith("?")) "Sure."
    else if (sentence.trim == "") "Fine. Be that way!"
    else "Whatever."
}

