<?php

function squareOfSums ($number) {
  $result = 0;
  for ($i = 0; $i <= $number; $i++) {
    $result += $i;
  }
  return $result ** 2;
}

function sumOfSquares ($number) {
  $result = 0;
  for ($i = 0; $i <= $number; $i++) {
    $result += $i ** 2;
  }
  return $result;
}

function difference ($number) {
  return squareOfSums($number) - sumOfSquares($number);
}

?>
