<?php

function distance($a, $b) {
  $a_len = strlen($a);
  if ($a_len != strlen($b)) {
    throw new InvalidArgumentException('DNA strands must be of equal length.');
  }
  $diff = 0;
  for ($i = 0; $i < $a_len; $i++) {
    if ($a[$i] != $b[$i]) {
      $diff += 1;
    }
  }
  return $diff;
}
