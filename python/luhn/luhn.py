
class Luhn():
    def __init__(self, number):
        self.number = number

    def addends(self):
        base = [int(x) for x in str(self.number)]
        return [(self._transform(n) if (i % 2 == 0) else n)
                for i, n in enumerate(base, start=len(base) % 2)]

    def _transform(self, n):
        if (n > 4):
            return 2 * n - 9
        else:
            return 2 * n

    def checksum(self):
        return sum(self.addends()) % 10

    def is_valid(self):
        return self.checksum() == 0

    @staticmethod
    def create(n):
        diff = (10 - Luhn(n * 10).checksum()) % 10
        return 10 * n + diff
