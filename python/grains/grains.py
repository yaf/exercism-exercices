
def on_square(number):
    if number <= 0:
        return 0
    if number == 1:
        return 1
    else:
        return on_square(number - 1) * 2

def total_after(number):
    return sum(on_square(x) for x in range(0, number + 1))
