
class SpaceAge():
    def __init__(self, age_in_second):
        self.seconds = age_in_second
        self.planets = {'on_earth': 31557600,
                'on_mercury': 7600530.24,
                'on_venus': 19413907.2,
                'on_mars': 59354294.4,
                'on_jupiter': 374335776,
                'on_saturn': 929596608,
                'on_uranus': 2661041808,
                'on_neptune': 5200418592,
                }

    def __getattr__(self, name):
        if name in self.planets:
            def func():
                return round((self.seconds / self.planets[name]), 2)
            return func
        else:
            raise AttributeError

