romans = ((10, "X"),
        (5, "V"),
        (4, "IV"),
        (1, "I"))

def numeral(number):
    result = ''
    for arabic, roman in romans:
        while number >= arabic:
            result = roman + result
            number -= arabic
    return result
