(ns beer)

(defn bottle-str [n]
  (str
    (cond
      (= 0 n) "no more bottles"
      (= 1 n) "1 bottle"
      :else (format "%s bottles" n))
    " of beer"))

(defn take-bottle [n]
  (cond
    (= 1 n) "it"
    :else "one"))

(defn print-bottle [n]
  (format "%s on the wall, %s.\nTake %s down and pass it around, %s on the wall.\n" (bottle-str n) (bottle-str n) (take-bottle n) (bottle-str (dec n))))

(defn verse [quantity] (print-bottle quantity))

(defn verse-to-sing
  ([start]
   (range 1 (inc start)))
  ([start stop]
   (range stop (inc start)))
  )

(defn sing
  ([start]
   (clojure.string/join
     [(clojure.string/join
       "\n"
       (reverse (map verse (verse-to-sing start)))
        )
      "\nNo more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n\n"
     ]
   ))
  ([start stop]
   (clojure.string/join
     [(clojure.string/join
        "\n"
        (reverse (map verse (verse-to-sing start stop)))
        )
      "\n"]
   ))
  )

