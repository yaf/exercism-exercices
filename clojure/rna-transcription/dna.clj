(ns dna (:require [clojure.string :as str]))

(defn to-rna [text] (str/replace text #"T" "U"))
