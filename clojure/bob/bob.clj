(ns bob
  (require [clojure.string :as str]))

(defn silent? [text] (str/blank? text))
(defn shout? [text] (= (str/upper-case text) text))
(defn question? [text]  (= \? (last text)))

(defn response-for [text]
  (cond
    (silent? text) "Fine. Be that way!"
    (shout? text) "Woah, chill out!"
    (question? text) "Sure."
    :else "Whatever."))

