(ns anagram (require [clojure.string :as str]))

(defn letters [word] (str/split (str/lower-case word) #""))
(defn sorted [letters]
  (sort
    (filter
      #(= (count %) 1)
      letters)))

(defn remove-same-word [word, words]
  (filter
    #(not= % word)
    words))

(defn anagrams-for [word, words]
  (filter
    #(=
       (sorted (letters %))
       (sorted (letters word)))
    (remove-same-word word words)))
