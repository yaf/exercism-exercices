(ns phrase (require [clojure.string :as str]))

(defn words [phrase]
  (map str/lower-case (re-seq #"\w+" phrase)))
(defn word-count [phrase] 
  (reduce 
    #(assoc %1 %2 (inc (%1 %2 0))) 
    {}
    (words phrase)))
