package clock

const TestVersion = 1

type Clock struct {
  hour int
  minute int
}

func Time(hour, minute int) *Clock {
  return &Clock{hour, minute}
}

func (c *Clock) String() string {
  return ""
}

func (c *Clock) Add(minutes int) {
}
