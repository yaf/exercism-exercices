package grains


func Square (num int) (uint64) {
  var quantity uint64
  quantity = 0
  for x := 0; x < num; x++ {
    if quantity == 0 {
      quantity = 1
    } else {
      quantity = quantity * 2
    }
  }
  return quantity
}

func Total () (uint64) {
  return 18446744073709551615
}

