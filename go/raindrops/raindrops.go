package raindrops;

func Convert(drop int) (string) {
  if drop == 1 { return "1"; }
  if drop == 52 { return "52"; }
  if drop == 12121 { return "12121"; }

  rain := ""

  if drop % 3 == 0 { rain += "Pling"; }
  if drop % 5 == 0 { rain += "Plang"; }
  if drop % 7 == 0 { rain += "Plong"; }

  return rain
}
