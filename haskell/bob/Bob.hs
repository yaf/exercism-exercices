module Bob (responseFor) where
import Data.Char

responseFor s
    | isBlank s = "Fine. Be that way."
    | isAllUpper s = "Woah, chill out!"
    | isQuestion s = "Sure."
    | otherwise = "Whatever."

isBlank = all isSpace

isAllUpper s = any isLetter s && not(any isLower s)

isQuestion s = last(s) == '?'
