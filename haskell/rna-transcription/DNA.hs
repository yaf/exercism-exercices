module DNA (toRNA) where

toRNA :: String -> String
toRNA x = map (goodLetter) x

goodLetter x
        | x == 'C' = 'C'
        | x == 'G' = 'G'
        | x == 'A' = 'A'
        | x == 'T' = 'U'
